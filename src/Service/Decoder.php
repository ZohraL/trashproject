<?php

namespace App\Service;

use App\Waste\CardWaste;
use App\Waste\GlassWaste;
use App\Waste\MetalWaste;
use App\Waste\OrganicWaste;
use App\Waste\PlasticWaste;
use App\Waste\RegularWaste;

class Decoder
{
    private $quartiers;
    private $services;

    public function __construct(string $fileName)
    {
        $data = json_decode(file_get_contents($fileName), true);
        $this->quartiers = $data['quartiers'];
        $this->services = $data['services'];
    }

    public function getWastes()

    {
        $glassWaste = new GlassWaste;
        $cardWaste = new CardWaste;
        $organicWaste = new OrganicWaste;
        $regularWaste = new RegularWaste;
        $metalWaste = new MetalWaste;
        foreach ($this->quartiers as $wastes) {

            $glassWaste->IncrementWaste($wastes['verre']);
            $cardWaste->IncrementWaste($wastes['papier']);
            $organicWaste->IncrementWaste($wastes['organique']);
            $regularWaste->IncrementWaste($wastes['autre']);
            $metalWaste->IncrementWaste($wastes['metaux']);
        }


        return [$glassWaste, $cardWaste, $organicWaste, $regularWaste, $metalWaste];
    }
}
