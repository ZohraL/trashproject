<?php

namespace App\Waste;

class Waste
{
    protected float $quantity = 0;

    public function IncrementWaste(float $weight)
    {
        $this->quantity += $weight;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
}
